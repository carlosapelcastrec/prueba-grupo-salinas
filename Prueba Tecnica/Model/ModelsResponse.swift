//
//  ModelsResponse.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

struct ProductResponseData : Codable{
    var codigo : String
    var mensaje: String
    var folio: String
    var advertencia: String
    var resultado : ProductData
}

struct ProductData : Codable{
    var paginacion : Pagination
    var categoria : String
    var productos : [Product?]
}
