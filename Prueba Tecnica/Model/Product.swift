//
//  Product.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

struct Product : Codable{
    var id : String?
    var idLinea : Int?
    var codigoCategoria : String?
    var idModalidad : Int?
    var relevancia : Int?
    var lineaCredito : String?
    var pagoSemanalPrincipal: Int?
    var plazoPrincipal : Int?
    var disponibleCredito: Bool?
    var abonosSemanales: [WeeklySubscriptions?]
    var sku : String?
    var nombre : String
    var urlImagenes: [String?]
    var precioRegular : Int?
    var precioFinal : Double?
    var porcentajeDescuento : Double?
    var descuento : Bool?
    var precioCredito: Double?
    var montoDescuento: Double?
}

struct WeeklySubscriptions : Codable{
    var plazo: Int
    var montoAbono: Int
    var montoDescuentoAbono: Int
    var montoUltimoAbono: Int
    var montoFinalCredito: Int
    var idPromocion: Int
    var montoDescuentoElektra: Int
    var montoDescuentoBanco: Int
    var precio: Int
    var montoAbonoDigital: Int
}
