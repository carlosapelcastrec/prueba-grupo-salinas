//
//  Pagination.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

struct Pagination: Codable{
    var pagina : Int
    var totalPaginas: Int
    var totalRegistros: Int
    var totalRegistrosPorPagina: Int
}
