//
//  SearchView.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import SwiftUI

struct SearchView: View {
    var animation: Namespace.ID
    
    @EnvironmentObject var productVM : ProductViewModel
    
    @FocusState var startTF: Bool
    
    var body: some View {
        
        VStack(spacing: 0){
            HStack(spacing: 20){
                
                Button {
                    withAnimation{
                        productVM.searchActivated = false
                    }
                    productVM.searchText = ""
                } label: {
                    Image(systemName: "arrow.left")
                        .font(.title2)
                        .foregroundColor(Color.black.opacity(0.7))
                }
                
                // Search Bar...
                HStack(spacing: 15){
                    Image(systemName: "magnifyingglass")
                        .font(.title2)
                        .foregroundColor(.gray)
                    
                    // Since we need a separate view for search bar....
                    TextField("Search", text: $productVM.searchText)
                        .focused($startTF)
                        .textCase(.lowercase)
                        .disableAutocorrection(true)
                }
                .padding(.vertical,12)
                .padding(.horizontal)
                .background(
                    
                    Capsule()
                        .strokeBorder(Color("Purple"),lineWidth: 1.5)
                )
                .matchedGeometryEffect(id: "SEARCHBAR", in: animation)
                .padding(.trailing,20)
            }
            .padding([.horizontal])
            .padding(.top)
            .padding(.bottom,10)
            
            // Showing Progress if searching...
            // else showing no results found if empty...
            if let products = productVM.searchedProducts{
                
                if products.isEmpty{
                    
                    // No Results Found....
                    VStack(spacing: 10){
                        
                        Image("NotFound")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .padding(.top,60)
                        
                        Text("Item Not Found")
                            //.font(.custom(customFont,size: 22).bold())
                        
                        Text("Try a more generic search term or try looking for alternative products.")
                            //.font(.custom(customFont,size: 16))
                            .foregroundColor(.gray)
                            .multilineTextAlignment(.center)
                            .padding(.horizontal,30)
                    }
                    .padding()
                }
                else{
                    // Filter Results....
                    ScrollView(.vertical, showsIndicators: false) {
                        
                        VStack(spacing: 0){
                            
                            // Found Text...
                            Text("Found \(products.count) results")
                                //.font(.custom(customFont, size: 24).bold())
                                .padding(.vertical)
                            
                            // Staggered Grid...
                            // See my Staggered Video..
                            // Link in Bio...
                            /*StaggeredGrid(columns: 2,spacing: 20, list: products) {product in
                                
                                // Card View....
                                ProductCardView(product: product)
                            }*/
                        }
                        .padding()
                    }
                }
            }
            else{
                
                ProgressView()
                    .padding(.top,30)
                    .opacity(productVM.searchText == "" ? 0 : 1)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity,alignment: .top)
        .background(
            
            Color("HomeBG")
                .ignoresSafeArea()
        )
        .onAppear {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                startTF = true
            }
        }
    }
    
    @ViewBuilder
    func ProductCardView(product: Product)->some View{
        
        VStack(spacing: 10){
            
            Image(product.urlImagenes[0] ?? "")
                .resizable()
                .aspectRatio(contentMode: .fit)
            // Moving image to top to look like its fixed at half top...
                .offset(y: -50)
                .padding(.bottom,-50)
            
            Text(product.nombre)
                //.font(.custom(customFont, size: 18))
                .fontWeight(.semibold)
                .padding(.top)
            
            if let price = product.precioFinal{
                Text("$ \(price)")
                    .foregroundColor(.gray)
            }else{
                Text("Precio No Especificado")
                    .foregroundColor(.gray)
            }
            
            if let price = product.precioCredito{
                Text("$ \(price)")
                    .foregroundColor(.gray)
            }else{
                Text("Precio No Especificado")
                    .foregroundColor(.gray)
            }
        }
        .padding(.horizontal,20)
        .padding(.bottom,22)
        .background(
            
            Color.white
                .cornerRadius(25)
        )
        .padding(.top,50)
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        ProductsView( product: .constant(Product(abonosSemanales: [], nombre: "Prueba", urlImagenes: [])))
    }
}



struct StaggeredGrid<Content: View,T: Identifiable>: View where T: Hashable {
    
    // It will return each object from collection to build View...
    var content: (T) -> Content
    
    var list: [T]
    
    // Columns...
    var columns: Int
    
    // Properties...
    var showsIndicators: Bool
    var spacing: CGFloat
    
    init(columns: Int,showsIndicators: Bool = false,spacing: CGFloat = 10,list: [T],@ViewBuilder content: @escaping (T)->Content){
        self.content = content
        self.list = list
        self.spacing = spacing
        self.showsIndicators = showsIndicators
        self.columns = columns
    }
    
    // Staggered Grid Function....
    func setUpList()->[[T]]{
        
        // creating empty sub arrays of columns count...
        var gridArray: [[T]] = Array(repeating: [], count: columns)
        
        // spiliting array for Vstack oriented View....
        var currentIndex: Int = 0
        
        for object in list{
            gridArray[currentIndex].append(object)
            
            // increasing index count...
            // and resetting if overbounds the columns count...
            if currentIndex == (columns - 1){
                currentIndex = 0
            }
            else{
                currentIndex += 1
            }
        }
        
        return gridArray
    }
    
    var body: some View {
        
        HStack(alignment: .top,spacing: 20){
            
            ForEach(setUpList(),id: \.self){columnsData in
                
                // For Optimized Using LazyStack...
                LazyVStack(spacing: spacing){
                    
                    ForEach(columnsData){object in
                        content(object)
                    }
                }
                .padding(.top,getIndex(values: columnsData) == 1 ? 80 : 0)
            }
        }
        // only vertical padding...
        // horizontal padding will be user's optional...
        .padding(.vertical)
    }
    
    // Moving Second row little Down....
    func getIndex(values: [T])->Int{
        
        let index = setUpList().firstIndex { t in
            return t == values
        } ?? 0
        
        return index
    }
}
