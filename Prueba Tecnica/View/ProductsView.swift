//
//  ProductsView.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import SwiftUI

struct ProductsView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @StateObject var productVM = ProductViewModel()
    @State var search = ""
    @State var columns = Array(repeating: GridItem(.flexible(), spacing: 15), count: 2)
    @Namespace var animation
    @Binding var product : Product?
    
    var body: some View{
        ScrollView(.vertical, showsIndicators: false) {
            LazyVStack{
                /*HStack{
                 Image(systemName: "arrow.left.circle")
                 Spacer()
                 ZStack{
                 if productVM.searchActivated{
                 SearchBar()
                 }
                 else{
                 SearchBar()
                 .matchedGeometryEffect(id: "SEARCHBAR", in: animation)
                 }
                 }
                 .frame(width: UIScreen.main.bounds.width / 1.6)
                 .padding(.horizontal,25)
                 .contentShape(Rectangle())
                 .onTapGesture {
                 withAnimation(.easeInOut){
                 productVM.searchActivated = true
                 }
                 }
                 
                 }
                 .padding(.horizontal)*/
                
                
                HStack{
                    Text("Popular")
                        .font(.title)
                        .fontWeight(.bold)
                    
                    Spacer()
                    
                    Button {
                        withAnimation(.easeOut){
                            if self.columns.count == 2{
                                self.columns.removeLast()
                            }
                            else{
                                
                                self.columns.append(GridItem(.flexible(), spacing: 15))
                            }
                        }
                    } label: {
                        Image(systemName: self.columns.count == 2 ? "rectangle.grid.1x2" : "square.grid.2x2")
                            .font(.system(size: 24))
                            .foregroundColor(.black)
                    }
                }
                .padding(.horizontal)
                .padding(.top,25)
                
                LazyVGrid(columns: self.columns,spacing: 25){
                    ForEach(productVM.products, id: \.id){product in
                        CardProduct(product: product,columns: self.$columns){
                            self.product = product
                            self.presentationMode.wrappedValue.dismiss()
                        }
                    }
                }
                .padding([.horizontal,.top])
            }
            .padding(.vertical)
        }
        .overlay(
            ZStack{
                if productVM.searchActivated{
                    SearchView(animation: animation)
                        .environmentObject(productVM)
                }
            }
        )
        .navigationBarBackButtonHidden(true)
        .onAppear(){
            productVM.getData()
        }
        .background(Color.black.opacity(0.05).edgesIgnoringSafeArea(.all))
    }
}

struct ProductsView_Previews: PreviewProvider {
    static var previews: some View {
        ProductsView( product: .constant(Product(abonosSemanales: [], nombre: "Prueba", urlImagenes: [])))
    }
}


@ViewBuilder
func SearchBar()->some View{
    HStack(spacing: 15){
        Image(systemName: "magnifyingglass")
            .font(.title2)
            .foregroundColor(.gray)
        TextField("Search", text: .constant(""))
            .disabled(true)
    }
    .padding(.vertical,12)
    .padding(.horizontal)
    .background(
        Capsule()
            .strokeBorder(Color.gray,lineWidth: 0.8)
    )
}
