//
//  SplashView.swift
//  Prueba Tecnica
//
//  Created by carlos on 24/08/22.
//

import SwiftUI

struct SplashView: View {
    var body: some View {
        ZStack{
            Color.customPrimary
                .edgesIgnoringSafeArea(.all)
            VStack{
                Image("logo")
                    .renderingMode(.template)
                    .foregroundColor(.white)
            }
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
