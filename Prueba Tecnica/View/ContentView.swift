//
//  ContentView.swift
//  Prueba Tecnica
//
//  Created by carlos on 24/08/22.
//

import SwiftUI

struct ContentView: View {
    @State var isActive:Bool = false
    
    var body: some View {
        if self.isActive {
            AnimatedSplash(color: Color.customPrimary, logo: "logo",animationTiming: 0.65) {
                MainView()
            }onAnimationEnd: {}
        } else {
            SplashView()
                .onAppear {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        withAnimation {
                            self.isActive = true
                        }
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
