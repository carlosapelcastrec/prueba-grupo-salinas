//
//  MainView.swift
//  Prueba Tecnica
//
//  Created by carlos on 24/08/22.
//

import SwiftUI

struct MainView: View {
    @State var index = 0
    @State var product : Product? = nil
    
    var body: some View {
        NavigationView{
            VStack{
                if let product = product{
                    VStack{
                        TabView(selection: self.$index){
                            ForEach(0 ..< product.urlImagenes.count, id: \.self){ index in
                                AsyncImage(url: URL(string: product.urlImagenes[index] ?? ""),
                                           content: { image in
                                    image.resizable()
                                        .aspectRatio(contentMode: .fit)
                                        .frame(height: self.index == index ?  230 : 180)
                                        .cornerRadius(15)
                                },
                                           placeholder: {
                                    ProgressView()
                                })
                                .padding(.horizontal)
                                .tag(index)
                            }
                        }
                        .frame(height: 230)
                        .padding(.top,25)
                        .tabViewStyle(PageTabViewStyle())
                        .animation(.easeOut)
                        VStack{
                            Text(product.nombre)
                                .font(.appFontTitle(size: 16))
                                .foregroundColor(.txtTertiary)
                                .multilineTextAlignment(.center)
                                .padding(.top)
                            VStack(alignment: .leading){
                                Text("SKU: " + (product.sku ?? ""))
                                    .font(.appFontTitle(size: 16))
                                    .foregroundColor(.txtQuaternary)
                                    .padding([.bottom,.top],4)
                                HStack{
                                    Text("Precio Regular: $\(product.precioRegular ?? 0)")
                                        .font(.appFontBody(size: 16))
                                        .foregroundColor(.txtTertiary)
                                    Text("- \((product.porcentajeDescuento ?? 0.0), specifier: "%.2f") %")
                                        .font(.appFontBody(size: 16))
                                        .foregroundColor(.txtSecondary)
                                    Spacer()
                                }
                                Text("Precio Final: $\((product.precioFinal ?? 0.0), specifier: "%.2f")")
                                    .font(.appFontBody(size: 16))
                                    .foregroundColor(.txtSecondary)
                                if product.disponibleCredito ?? false {
                                    Text("Precio a Credito: $\((product.precioCredito ?? 0.0), specifier: "%.2f")")
                                        .font(.appFontBody(size: 16))
                                        .foregroundColor(.txtTertiary)
                                        .padding(.bottom)
                                }
                            }
                            .padding(.horizontal,6)
                        }
                        .padding(.vertical)
                    }
                }
                Spacer()
                HStack{
                    if product != nil{
                        Button(action: {
                            self.product = nil
                        }){
                            HStack {
                                Image(systemName: "clear")
                                Text("Limpiar")
                            }
                            .frame(width: 150)
                            .font(.appFontBody(size: 16))
                            .padding()
                            .foregroundColor(.txtSecondary)
                            .cornerRadius(40)
                            .overlay(
                                RoundedRectangle(cornerRadius: 40)
                                    .stroke(Color.customPrimary, lineWidth: 3)
                            )
                        }
                    }
                    NavigationLink(destination: ProductsView(product: $product)){
                        HStack {
                            Image(systemName: "magazine")
                            Text("Productos")
                        }
                        .frame(width: 150)
                        .font(.appFontBody(size: 16))
                        .padding()
                        .foregroundColor(.txtPrimary)
                        .background(Color.customPrimary)
                        .cornerRadius(40)
                    }
                }
                Spacer()
            }
        }
        .onAppear {
            setupAppearance()
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

func setupAppearance() {
    UIPageControl.appearance().currentPageIndicatorTintColor = .red
    UIPageControl.appearance().pageIndicatorTintColor = UIColor.red.withAlphaComponent(0.2)
}
