//
//  AnimatedSplash.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import SwiftUI

struct AnimatedSplash<Content: View>: View {
    var content: Content
    var color: Color
    var logo: String
    var barHeight: CGFloat = 60
    var animationTiming: Double = 0.65
    var onAnimationEnd: ()->()
    init(color: Color,logo: String,barHeight: CGFloat = 60,animationTiming: Double = 0.65,@ViewBuilder content: @escaping ()->Content,onAnimationEnd: @escaping ()->()){
        self.content = content()
        self.onAnimationEnd = onAnimationEnd
        self.color = color
        self.logo = logo
        self.barHeight = barHeight
        self.animationTiming = animationTiming
    }
    @State var startAnimation: Bool = false
    @State var animateContent: Bool = false
    @Namespace var animation
    
    @State var disableControls: Bool = true
    var body: some View {
        VStack(spacing: 0){
            if startAnimation{
                GeometryReader{proxy in
                    let size = proxy.size
                    
                    VStack(spacing: 0){
                        ZStack(alignment: .bottom){
                            Rectangle()
                                .fill(color)
                                .matchedGeometryEffect(id: "SPLASHCOLOR", in: animation)
                                .frame(height: barHeight + safeArea().top)
                            
                            Image("logo")
                                .resizable()
                                .renderingMode(.template)
                                .foregroundColor(.white)
                                .aspectRatio(contentMode: .fit)
                                .matchedGeometryEffect(id: "SPLASHICON", in: animation)
                                .frame(width: 65, height: 65)
                                .padding(.bottom,10)
                        }
                        
                        content
                            .offset(y: animateContent ? 0 : (size.height - (barHeight + safeArea().top)))
                            .disabled(disableControls)
                    }
                    .frame(maxHeight: .infinity,alignment: .top)
                }
                .transition(.identity)
                .ignoresSafeArea(.container, edges: .all)
                .onAppear {
                    if !animateContent{
                        withAnimation(.easeInOut(duration: animationTiming)){
                            animateContent = true
                        }
                    }
                }
            }else{
                ZStack{
                    Rectangle()
                        .fill(color)
                        .matchedGeometryEffect(id: "SPLASHCOLOR", in: animation)
                    Image("logo")
                        .matchedGeometryEffect(id: "SPLASHICON", in: animation)
                }
                .ignoresSafeArea(.container, edges: .all)
            }
        }
        .onAppear {
            if !startAnimation{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.15){
                    withAnimation(.easeInOut(duration: animationTiming)){
                        startAnimation = true
                    }
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + (animationTiming - 0.05)){
                    disableControls = false
                    onAnimationEnd()
                }
            }
        }
    }
}

struct AnimatedSplash_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
