//
//  CardProduct.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import SwiftUI

struct CardProduct: View {
    @StateObject var productVM = ProductViewModel()
    var product : Product
    @Binding var columns : [GridItem]
    @Namespace var namespace
    var action : () -> Void
    
    var body: some View{
        VStack{
            if self.columns.count == 2{
                VStack(spacing: 15){
                    ZStack(alignment: Alignment(horizontal: .trailing, vertical: .top)) {
                        AsyncImage(url: URL(string: product.urlImagenes[0] ?? "")) { phase in
                            switch phase {
                            case .empty:
                                ProgressView()
                            case .success(let image):
                                image.resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(height: 250)
                                    .cornerRadius(15)
                            case .failure:
                                Image("image_not_available")
                                    .resizable()
                                    .frame(height: 250)
                                    .cornerRadius(15)
                            @unknown default:
                                EmptyView()
                            }
                        }
                        /*Button {
                         } label: {
                         
                         Image(systemName: "heart.fill")
                         .foregroundColor(.red)
                         .padding(.all,10)
                         .background(Color.white)
                         .clipShape(Circle())
                         }
                         .padding(.all,10)*/
                    }
                    .matchedGeometryEffect(id: "image", in: self.namespace)
                    
                    Text(product.nombre)
                        .font(.appFontTitle(size: 14))
                        .foregroundColor(.txtTertiary)
                        .multilineTextAlignment(.center)
                        .padding(.top)
                    
                    if let price = product.precioRegular{
                        HStack{
                            Text("$ \(price)")
                                .font(.appFontBody(size: 16))
                                .foregroundColor(.txtTertiary)
                            if let porcentage = product.porcentajeDescuento{
                                Text("- \(porcentage, specifier: "%.2f") %")
                                    .font(.appFontBody(size: 16))
                                    .foregroundColor(.txtSecondary)
                            }
                            Spacer()
                        }
                    }else{
                        Text("Precio No Especificado")
                            .font(.appFontBody(size: 16))
                            .foregroundColor(.txtTertiary)
                    }
                    
                    if let price = product.precioFinal{
                        Text("$ \(price, specifier: "%.2f")")
                            .font(.appFontBody(size: 16))
                            .foregroundColor(.txtSecondary)
                    }else{
                        Text("Precio No Especificado")
                            .font(.appFontBody(size: 16))
                            .foregroundColor(.txtTertiary)
                    }
                    
                    Button (action: action){
                        Text("Ver Detalles")
                            .foregroundColor(.txtPrimary)
                            .font(.appFontBody(size: 16))
                            .padding(.vertical,10)
                            .padding(.horizontal,25)
                            .background(Color.customPrimary)
                            .cornerRadius(10)
                    }
                    .matchedGeometryEffect(id: "Details", in: self.namespace)
                }
            }
            else{
                HStack(spacing: 15){
                    ZStack(alignment: Alignment(horizontal: .trailing, vertical: .top)) {
                        AsyncImage(url: URL(string: product.urlImagenes[0] ?? "")) { phase in
                            switch phase {
                            case .empty:
                                ProgressView()
                            case .success(let image):
                                image.resizable()
                                    .aspectRatio(contentMode: .fit)
                                    .frame(width: (UIScreen.main.bounds.width - 45) / 2,height: 250)
                                    .cornerRadius(15)
                            case .failure:
                                Image("image_not_available")
                                    .resizable()
                                    .frame(height: 250)
                                    .cornerRadius(15)
                            @unknown default:
                                EmptyView()
                            }
                        }
                        /*Button {
                         
                         } label: {
                         
                         Image(systemName: "heart.fill")
                         .foregroundColor(.red)
                         .padding(.all,10)
                         .background(Color.white)
                         .clipShape(Circle())
                         }
                         .padding(.all,10)
                         */
                    }
                    .matchedGeometryEffect(id: "image", in: self.namespace)
                    
                    VStack(alignment: .leading, spacing: 10) {
                        
                        Text(product.nombre)
                            .font(.appFontTitle(size: 14))
                            .foregroundColor(.txtTertiary)
                            .multilineTextAlignment(.leading)
                        
                        Text(productVM.getCategory(category : product.codigoCategoria ?? ""))
                            .font(.appFontBody(size: 14))
                            .foregroundColor(.txtQuaternary)
                            .multilineTextAlignment(.leading)
                        
                        if let price = product.precioRegular{
                            Text("$ \(price)")
                                .font(.appFontBody(size: 16))
                                .foregroundColor(.txtTertiary)
                            if let porcentage = product.porcentajeDescuento{
                                Text("- \(porcentage, specifier: "%.2f") %")
                                    .font(.appFontBody(size: 16))
                                    .foregroundColor(.txtSecondary)
                            }
                        }else{
                            Text("Precio No Especificado")
                                .font(.appFontBody(size: 16))
                                .foregroundColor(.txtTertiary)
                        }
                        
                        if let price = product.precioFinal{
                            Text("$ \(price, specifier: "%.2f")")
                                .font(.appFontBody(size: 16))
                                .foregroundColor(.txtSecondary)
                        }else{
                            Text("Precio No Especificado")
                                .font(.appFontBody(size: 16))
                                .foregroundColor(.txtTertiary)
                        }
                        
                        Button (action: action){
                            Text("Ver Detalles")
                                .foregroundColor(.txtPrimary)
                                .font(.appFontBody(size: 16))
                                .padding(.vertical,10)
                                .padding(.horizontal,25)
                                .background(Color.customPrimary)
                                .cornerRadius(10)
                        }
                        .padding(.top,10)
                        .matchedGeometryEffect(id: "Details", in: self.namespace)
                    }
                }
                .padding(.trailing)
            }
        }
        .padding()
        .background(.white)
        .cornerRadius(15)
    }
}

struct CardProduct_Previews: PreviewProvider {
    static var previews: some View {
        CardProduct(product: Product(abonosSemanales: [], nombre: "Prueba", urlImagenes: []), columns: .constant(Array(repeating: GridItem(.flexible(), spacing: 15), count: 2)), action: {})
    }
}
