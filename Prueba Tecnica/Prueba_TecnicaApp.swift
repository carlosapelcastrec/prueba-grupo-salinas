//
//  Prueba_TecnicaApp.swift
//  Prueba Tecnica
//
//  Created by carlos on 24/08/22.
//

import SwiftUI

@main
struct Prueba_TecnicaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
