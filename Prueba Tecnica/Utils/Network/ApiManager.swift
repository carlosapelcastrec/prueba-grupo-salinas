//
//  ApiManager.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

class ApiManager{
    
    static func getProducts(completion:@escaping (ProductResponseData?, String?)->Void){
        guard let url = URL(string: Config.base + Urls.GET_PRODUCTS) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completion(nil, error?.localizedDescription)
                return
            }
            guard let data = data else {
                return
            }
            guard let response = response as? HTTPURLResponse, (200 ..< 299) ~= response.statusCode else {
                completion(nil, "Error en servicio")
                return
            }
            do {
             let decoder = JSONDecoder()
             decoder.keyDecodingStrategy = .convertFromSnakeCase
             let result = try decoder.decode(ProductResponseData.self, from: data)
                completion(result,nil)
             } catch {
                 print("Error: Cannot convert data to JSON object\(error)")
             }
        }.resume()
    }
}
