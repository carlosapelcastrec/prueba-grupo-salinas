//
//  AppConstants.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

enum Config {
    static let base = "http://alb-dev-ekt-875108740.us-east-1.elb.amazonaws.com/sapp"
}


enum Urls {
    static let GET_PRODUCTS = "/productos/plp/1/ad2fdd4bbaec4d15aa610a884f02c91a"
}
