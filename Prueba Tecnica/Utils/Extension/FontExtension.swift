//
//  FontExtension.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 26/08/22.
//

import SwiftUI

extension Font {
    static func appFontTitle(size: CGFloat) -> Font {
        return Font.custom("BandrekDemo", size: size)
    }
    
    static func appFontBody(size: CGFloat) -> Font {
        return Font.custom("Naturalist", size: size)
    }
}
