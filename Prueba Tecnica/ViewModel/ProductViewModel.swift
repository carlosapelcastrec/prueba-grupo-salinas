//
//  ProductViewModel.swift
//  Prueba Tecnica
//
//  Created by Carlos A Pelcastre Carmona on 25/08/22.
//

import Foundation

class ProductViewModel : ObservableObject{
    @Published var products = [Product]()
    
    @Published var searchText: String = ""
    @Published var searchActivated: Bool = false
    @Published var searchedProducts: [Product]?
    
    func getData(){
        getProducts()
    }
    
    func getProducts(){
        ApiManager.getProducts { data, error in
            self.products = data?.resultado.productos as! [Product]
            print("error:" + (error ?? ""))
        }
    }
    
    func getCategory(category : String) -> String{
        switch category{
        case "TL" :
            return "Celulares"
        case "C" :
            return "Tv y Video"
        case "MA" :
            return "Motos"
        default:
            return "No Especificado"
        }
    }
}
